package day1;

import util.FileReader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ReportRepair {
    public static final int TARGET_SUM = 2020;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var list = fileReader.readFile("./src/day1/reportRepair.txt");

        part1(list);
        part2(list);
    }

    private static void part1(List<String> data) {
        // convert to set
        var set = new HashSet<Integer>();
        data.forEach(val -> set.add(Integer.valueOf(val)));

        // iterate set and get result
        for (Integer targetVal1 : set) {
            var targetVal2 = TARGET_SUM - targetVal1;
            if (set.contains(targetVal2)) {
                System.out.println("#1 Found result: " + targetVal1 * targetVal2);
                System.out.println("  check: " + targetVal1 + "+" + targetVal2 + " = " + (targetVal1 + targetVal2));
                break;
            }
        }
    }

    private static void part2(List<String> data) {
        // convert to map
        var map = new HashMap<Integer, Integer[]>(); // key sum, value pair of values
        for (var val : data) {
            for (var val2 : data) {
                var int1 = Integer.parseInt(val);
                var int2 = Integer.parseInt(val2);
                map.put(int1 + int2, new Integer[]{int1, int2});
            }
        }

        // iterate map and get result
        for (var val : map.entrySet()) {
            var targetVal1 = val.getValue()[0];
            var val2 = TARGET_SUM - targetVal1;
            if (map.containsKey(val2)) {
                var targetVal2 = map.get(val2)[0];
                var targetVal3 = map.get(val2)[1];
                System.out.println("#2 Found result: " + targetVal1 * targetVal2 * targetVal3);
                System.out.println("  check: " + targetVal1 + "+" + targetVal2 + "+" + targetVal3 + " = " + (targetVal1 + targetVal2 + targetVal3));
                break;
            }
        }
    }
}
