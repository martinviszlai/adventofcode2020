package day19;

import util.FileReader;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class MonsterMessages {
    private static final String WRAPPER = "#";

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var messages = fileReader.readFile("./src/day19/monsterMessages_messages.txt");
        var rulesLines = fileReader.readFile("./src/day19/monsterMessages_rules.txt");
        var rules = parseRules(rulesLines);

        part1(messages, rules);
        part2(messages, rules);
    }

    private static void part1(List<String> messages, Map<Integer, String> rules) {
        var regex = convertRulesToRegex(rules).get(0);
        long matching = countMatchingMessages(messages, regex);
        System.out.println("#1 Result: " + matching);
    }

    private static void part2(List<String> messages, Map<Integer, String> rules) {
        // overwrite rules
        rules.put(8, "42 +");
        // as java regex does not support recursion, have to make this awful workaround
        // if it would regex should look like "42 (?R)? 31"
        rules.put(11, "42 31 | 42 42 31 31 | 42 42 42 31 31 31 | 42 42 42 42 31 31 31 31");

        var regex = convertRulesToRegex(rules).get(0);
        long matching = countMatchingMessages(messages, regex);
        System.out.println("#2 Result: " + matching);
    }


    private static Map<Integer, String> convertRulesToRegex(Map<Integer, String> rulesMap) {
        var regexMap = new HashMap<Integer, String>();

        // wrap every rule reference with WRAPPER
        for (int i = 0; i < rulesMap.size(); i++) {
            regexMap.put(i, WRAPPER + rulesMap.get(i).replace(" ", WRAPPER + WRAPPER) + WRAPPER);
        }

        // iterate over map and replace each rule with its definition
        for (int i = 0; i < regexMap.size(); i++) {
            var expandedRule = regexMap.get(i);
            var ruleIndex = "" + i;
            for (int j = 0; j < regexMap.size(); j++) {
                var ruleToUpdated = regexMap.get(j);
                var target = WRAPPER + ruleIndex + WRAPPER;
                var replacement = "(" + expandedRule + ")";
                var newRuleDef = ruleToUpdated.replace(target, replacement);
                regexMap.put(j, newRuleDef);
            }
        }

        // remove helper WRAPPER and unnecessary brackets
        for (int i = 0; i < regexMap.size(); i++) {
            regexMap.put(i, "^" + regexMap.get(i)
                    .replace(WRAPPER, "")
                    .replace("(a)", "a")
                    .replace("(b)", "b")
                    + "$");
        }

        return regexMap;
    }

    private static long countMatchingMessages(List<String> messages, String regex) {
        var pattern = Pattern.compile(regex);
        return messages.stream()
                .filter(message -> pattern.matcher(message).matches())
                .count();
    }

    private static Map<Integer, String> parseRules(List<String> rules) {
        var rulesMap = new HashMap<Integer, String>();

        rules.forEach(ruleDef -> {
            var def = parseEntryFromRuleDef(ruleDef);
            if (ruleDef.contains("a") || ruleDef.contains("b")) {
                rulesMap.put(def.getKey(), def.getValue().substring(1, 2));
            } else {
                rulesMap.put(def.getKey(), def.getValue());
            }
        });
        return rulesMap;
    }

    private static Map.Entry<Integer, String> parseEntryFromRuleDef(String ruleDef) {
        var matcher = Pattern.compile("(\\d+): (.+)").matcher(ruleDef);
        if (matcher.matches()) {
            return new AbstractMap.SimpleEntry<>(
                    Integer.parseInt(matcher.group(1)),
                    matcher.group(2)
            );
        }
        return null;
    }
}
