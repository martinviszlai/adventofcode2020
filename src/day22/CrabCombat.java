package day22;

import util.FileReader;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class CrabCombat {
    public static final List<Integer> player1deck = new LinkedList();
    public static final List<Integer> player2deck = new LinkedList();


    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day22/crabCombat.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> lines) {
        loadDecks(lines);
        var winnersDeck = playCombat();
        long score = calculateScore(winnersDeck);
        System.out.println("#1 Result: " + score);
    }

    private static void part2(List<String> lines) {
        loadDecks(lines);
        var winner = playRecursiveCombat(player1deck, player2deck);
        var winnersDeck = winner == 1 ? player1deck : player2deck;
        long score = calculateScore(winnersDeck);
        System.out.println("#2 Result: " + score);
    }

    private static long calculateScore(List<Integer> winnersDeck) {
        var score = 0L;
        for (int index = 0; index < winnersDeck.size(); index++) {
            score += winnersDeck.get(index) * (winnersDeck.size() - index);
        }
        return score;
    }

    private static int playRecursiveCombat(List<Integer> player1deck, List<Integer> player2deck) {
        var player1history = new HashSet<List<Integer>>();
        var player2history = new HashSet<List<Integer>>();

        while (!player1deck.isEmpty() && !player2deck.isEmpty()) {
            // check history
            if (!player1history.add(player1deck) && !player2history.add(player2deck)) {
                return 1; // rule 1 - a player already had this hand in this game
            }

            // draw top cards
            var player1card = player1deck.remove(0);
            var player2card = player2deck.remove(0);

            // decide winner
            var winnerCard = 0;
            var winner = 0;
            if (player1card <= player1deck.size() && player2card <= player2deck.size()) {
                winner = playRecursiveCombat(new LinkedList<>(player1deck.subList(0, player1card)),
                        new LinkedList<>(player2deck.subList(0, player2card)));
                winnerCard = (winner == 1) ? player1card : player2card;
            } else {
                winner = player1card > player2card ? 1 : 2;
                winnerCard = (winner == 1) ? player1card : player2card;

            }

            var winnerDeck = (winner == 1 )? player1deck : player2deck;

            winnerDeck.add(winnerCard);
            winnerDeck.add(winnerCard == player1card ? player2card : player1card);
        }

        return player1deck.isEmpty() ? 2 : 1;
    }

    private static List<Integer> playCombat() {
        while (!player1deck.isEmpty() && !player2deck.isEmpty()) {
            var player1card = player1deck.get(0);
            player1deck.remove(0);
            var player2card = player2deck.get(0);
            player2deck.remove(0);

            if (player1card > player2card) {
                player1deck.add(player1card);
                player1deck.add(player2card);
            } else {
                player2deck.add(player2card);
                player2deck.add(player1card);
            }
        }

        return player1deck.isEmpty() ? player2deck : player1deck;
    }

    private static void loadDecks(List<String> lines) {
        player1deck.clear();
        player2deck.clear();
        var activeDeck = player1deck;
        for (String line : lines) {
            if (line.contains("Player")) {
                continue;
            }
            if (line.isBlank()) {
                activeDeck = player2deck;
            } else {
                activeDeck.add(Integer.parseInt(line));
            }
        }
    }
}
