package day17;

import util.FileReader;

import java.util.List;

public class ConwayCubes {
    public static final char ACTIVE = '#';
    public static final char INACTIVE = '.';
    public static final int CYCLES_COUNT = 6;
    public static final int MAX_SIZE = 25;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day17/conwayCubes.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> lines) {
        var pocketDimension = new char[MAX_SIZE][MAX_SIZE][MAX_SIZE];
        var startPosition = 8;

        // init
        for (int y = 0; y < lines.size(); y++) {
            var cubes = lines.get(y).toCharArray();
            for (int x = 0; x < cubes.length; x++) {
                var xx = x + startPosition;
                var yy = y + startPosition;
                pocketDimension[xx][yy][MAX_SIZE / 2] = cubes[x];
            }
        }

        for (int cycle = 0; cycle < CYCLES_COUNT; cycle++) {
            pocketDimension = performCycle(pocketDimension);
        }

        System.out.println("#1 Result: " + getResult(pocketDimension));
    }

    private static void part2(List<String> lines) {
        var pocketDimension = new char[MAX_SIZE][MAX_SIZE][MAX_SIZE][MAX_SIZE];
        var startPosition = 8;

        // init
        for (int y = 0; y < lines.size(); y++) {
            var cubes = lines.get(y).toCharArray();
            for (int x = 0; x < cubes.length; x++) {
                var xx = x + startPosition;
                var yy = y + startPosition;
                pocketDimension[xx][yy][MAX_SIZE / 2][MAX_SIZE / 2] = cubes[x];
            }
        }

        for (int cycle = 0; cycle < CYCLES_COUNT; cycle++) {
            pocketDimension = performCycle(pocketDimension);
        }

        System.out.println("#2 Result: " + getResult(pocketDimension));
    }

    private static int getResult(char[][][] pocketDimension) {
        var active = 0;
        for (var x = 0; x < MAX_SIZE; x++) {
            for (var y = 0; y < MAX_SIZE; y++) {
                for (var z = 0; z < MAX_SIZE; z++) {
                    if (isCubeActive(x, y, z, pocketDimension)) {
                        active++;
                    }
                }
            }
        }
        return active;
    }

    private static char[][][] performCycle(char[][][] before) {
        var after = new char[MAX_SIZE][MAX_SIZE][MAX_SIZE];
        for (var z = 0; z < MAX_SIZE; z++) {
            for (var y = 0; y < MAX_SIZE; y++) {
                for (var x = 0; x < MAX_SIZE; x++) {
                    var activeNeighbours = countActiveNeighbours(x, y, z, before);
                    if (isCubeActive(x, y, z, before) && activeNeighbours == 2 || activeNeighbours == 3) {
                        after[x][y][z] = ACTIVE;
                    } else {
                        after[x][y][z] = INACTIVE;
                    }
                }
            }
        }
        return after;
    }

    private static int countActiveNeighbours(int x, int y, int z, char[][][] pocketDimension) {
        var activeNeighbours = 0;
        for (var nx = x - 1; nx <= x + 1; nx++) {
            for (var ny = y - 1; ny <= y + 1; ny++) {
                for (var nz = z - 1; nz <= z + 1; nz++) {
                    if (nx == x && ny == y && nz == z) {
                        continue;
                    }
                    if (isCubeActive(nx, ny, nz, pocketDimension)) {
                        activeNeighbours++;
                    }
                }
            }
        }
        return activeNeighbours;
    }

    private static boolean isCubeActive(int x, int y, int z, char[][][] pocketDimension) {
        if (x < 0 || y < 0 || z < 0 || x >= MAX_SIZE || y >= MAX_SIZE || z >= MAX_SIZE) return false;
        return pocketDimension[x][y][z] == ACTIVE;
    }


    private static int getResult(char[][][][] pocketDimension) {
        var active = 0;
        for (var x = 0; x < MAX_SIZE; x++) {
            for (var y = 0; y < MAX_SIZE; y++) {
                for (var z = 0; z < MAX_SIZE; z++) {
                    for (var w = 0; w < MAX_SIZE; w++) {
                        if (isCubeActive(x, y, z, w, pocketDimension)) {
                            active++;
                        }
                    }
                }
            }
        }
        return active;
    }

    private static char[][][][] performCycle(char[][][][] before) {
        var after = new char[MAX_SIZE][MAX_SIZE][MAX_SIZE][MAX_SIZE];
        for (var z = 0; z < MAX_SIZE; z++) {
            for (var y = 0; y < MAX_SIZE; y++) {
                for (var x = 0; x < MAX_SIZE; x++) {
                    for (var w = 0; w < MAX_SIZE; w++) {
                        var activeNeighbours = countActiveNeighbours(x, y, z, w, before);
                        if (isCubeActive(x, y, z, w, before) && activeNeighbours == 2 || activeNeighbours == 3) {
                            after[x][y][z][w] = ACTIVE;
                        } else {
                            after[x][y][z][w] = INACTIVE;
                        }
                    }
                }
            }
        }
        return after;
    }

    private static int countActiveNeighbours(int x, int y, int z, int w, char[][][][] pocketDimension) {
        var activeNeighbours = 0;
        for (var nx = x - 1; nx <= x + 1; nx++) {
            for (var ny = y - 1; ny <= y + 1; ny++) {
                for (var nz = z - 1; nz <= z + 1; nz++) {
                    for (var nw = w - 1; nw <= w + 1; nw++) {
                        if (nx == x && ny == y && nz == z && nw == w) {
                            continue;
                        }
                        if (isCubeActive(nx, ny, nz, nw, pocketDimension)) {
                            activeNeighbours++;
                        }
                    }
                }
            }
        }
        return activeNeighbours;
    }

    private static boolean isCubeActive(int x, int y, int z, int w, char[][][][] pocketDimension) {
        if (x < 0 || y < 0 || z < 0 || w < 0 || x >= MAX_SIZE || y >= MAX_SIZE || z >= MAX_SIZE || w >= MAX_SIZE)
            return false;
        return pocketDimension[x][y][z][w] == ACTIVE;
    }


}
