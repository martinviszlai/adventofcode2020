package day7;

import util.FileReader;

import java.util.*;
import java.util.regex.Pattern;

public class HandyHaversacks {

    public static final String MY_BAG = "shiny gold";

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day7/handyHaversacks.txt");
        var bagRules = loadBagsRestrictions(lines);

        part1(bagRules);
        part2(bagRules);
    }

    private static void part1(Map<String, Bag> rules) {
        var parentCount = flatParents(rules.get(MY_BAG)).size();

        System.out.println("#1 Result: " + parentCount);
    }

    private static void part2(Map<String, Bag> rules) {
        var sum = bagCountWithinBag(rules.get(MY_BAG), 1);

        System.out.println("#2 Result: " + sum);
    }

    private static Collection<Bag> flatParents(Bag bag) {
        var parents = new HashSet<Bag>();
        for (var parent : bag.parents) {
            parents.add(parent);
            parents.addAll(flatParents(parent));
        }
        return parents;
    }

    private static int bagCountWithinBag(Bag bag, int multiplier) {
        var sum = 0;
        for (var child : bag.children.entrySet()) {
            sum += multiplier * child.getValue();
            sum += bagCountWithinBag(child.getKey(), multiplier * child.getValue());
        }
        return sum;
    }

    private static Map<String, Bag> loadBagsRestrictions(List<String> data) {
        var bagRules = new HashMap<String, Bag>();
        data.forEach(line -> {
            var split = line.split("bags contain");
            var parentBagColor = split[0].trim();

            var currentBag = bagRules.getOrDefault(parentBagColor, new Bag(parentBagColor));

            if (!split[1].contains("no other")) {
                var rules = split[1].split(",");
                for (String rule : rules) {
                    var matcher = Pattern.compile("(\\d+) (\\w+\\s\\w+) bag.*").matcher(rule.trim());
                    if (matcher.matches()) {
                        var quantity = Integer.parseInt(matcher.group(1));
                        var color = matcher.group(2);
                        var bag = bagRules.getOrDefault(color, new Bag(color));
                        currentBag.children.put(bag, quantity);
                        bag.parents.add(currentBag);
                        bagRules.putIfAbsent(color, bag);
                    }
                }
            }
            bagRules.put(parentBagColor, currentBag);
        });
        return bagRules;
    }

    private static class Bag {
        String color;
        Set<Bag> parents = new HashSet<>();
        Map<Bag, Integer> children = new HashMap<>();

        public Bag(String color) {
            this.color = color;
        }
    }

}
