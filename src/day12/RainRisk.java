package day12;

import util.FileReader;

import java.util.List;

public class RainRisk {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day12/rainRisk.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> data) {
        var ship = new Ship();
        data.forEach(instruction -> {
            var movement = instruction.substring(0, 1);
            var value = Integer.parseInt(instruction.substring(1));
            switch (movement) {
                case "F" -> ship.moveForward(value);
                case "L" -> ship.turn(360-value);
                case "R" -> ship.turn(value);
                default -> ship.move(Direction.valueOf(movement), value);
            }
        });
        System.out.println("#1 Result: " + (Math.abs(ship.positionX) + Math.abs(ship.positionY)));
    }


    private static void part2(List<String> data) {
        var ship = new Ship();
        var waypoint = new Waypoint();
        data.forEach(instruction -> {
            var movement = instruction.substring(0, 1);
            var value = Integer.parseInt(instruction.substring(1));
            switch (movement) {
                case "F" -> ship.moveToWaypoint(waypoint, value);
                case "L" -> waypoint.turn(360-value);
                case "R" -> waypoint.turn(value);
                default -> waypoint.move(Direction.valueOf(movement), value);
            }
        });
        System.out.println("#2 Result: " + (Math.abs(ship.positionX) + Math.abs(ship.positionY)));
    }

    private enum Direction {
        N(0),
        E(90),
        S(180),
        W(270);
        int azimuth;

        Direction(int azimuth) {
            this.azimuth = azimuth;
        }

        public static Direction byAzimuth(int azimuth) {
            for (Direction direction : values()) {
                if (direction.azimuth == azimuth % 360) {
                    return direction;
                }
            }
            return null;
        }
    }

    private static class Ship {
        Direction direction = Direction.E;
        int positionX = 0;
        int positionY = 0;

        public void moveForward(int distance) {
            move(this.direction, distance);
        }

        public void turn(int degrees) {
            direction = Direction.byAzimuth(direction.azimuth + degrees);
        }

        public void move(Direction direction, int distance) {
            switch (direction) {
                case N -> positionY -= distance;
                case E -> positionX += distance;
                case S -> positionY += distance;
                case W -> positionX -= distance;
            }
        }

        public void moveToWaypoint(Waypoint waypoint, int distance) {
            positionX += distance * waypoint.positionX;
            positionY += distance * waypoint.positionY;
        }
    }

    private static class Waypoint {
        int positionX = 10;
        int positionY = 1;

        public void turn(int degrees) {
            while (degrees > 0) {
                // switch x,y
                positionX = positionX + positionY;
                positionY = positionX - positionY;
                positionX = positionX - positionY;
                // make y negative
                positionY = -positionY;
                degrees -= 90;
            }
        }

        public void move(Direction direction, int distance) {
            switch (direction) {
                case N -> positionY += distance;
                case E -> positionX += distance;
                case S -> positionY -= distance;
                case W -> positionX -= distance;
            }
        }
    }
}
