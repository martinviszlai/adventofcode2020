package day5;

import util.FileReader;

import java.util.List;
import java.util.stream.Collectors;

public class BinaryBoarding {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day5/binaryBoarding.txt");
        var boardingPasses = loadBoardingPasses(lines);

        part1(boardingPasses);
        part2(boardingPasses);
    }

    private static void part1(List<BoardingPass> data) {
        var maxCode = data.stream().mapToInt(bp -> bp.seatId).max().orElse(0);

        System.out.println("#1 Result: " + maxCode);
    }

    private static void part2(List<BoardingPass> data) {
        var minCode = data.stream().mapToInt(bp -> bp.seatId).min().orElse(0);
        var maxCode = data.stream().mapToInt(bp -> bp.seatId).max().orElse(0);
        var keySet = data.stream().map(bp -> bp.seatId).collect(Collectors.toSet());

        for (int index = minCode; index < maxCode; index++) {
            if (!keySet.contains(index)) {
                System.out.println("#2 Result: " + index);
                break;
            }
        }
    }

    private static List<BoardingPass> loadBoardingPasses(List<String> data) {
        return data.stream().map(BoardingPass::new).collect(Collectors.toList());
    }

    private static class BoardingPass {
        String code;
        int seatId;
        int row;
        int column;

        public BoardingPass(String code) {
            var codeBinary = code
                    .replaceAll("[FL]", "0")
                    .replaceAll("[BR]", "1");
            this.code = code;
            this.row = Integer.parseInt(codeBinary.substring(0, 7), 2);
            this.column = Integer.parseInt(codeBinary.substring(7, 10), 2);
            this.seatId = row * 8 + column;
        }
    }
}
