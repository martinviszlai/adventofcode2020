package day20;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Tile {
    static final int TILE_SIZE = 10;
    static final int TOP = 0;
    static final int RIGHT = 1;
    static final int BOTTOM = 2;
    static final int LEFT = 3;

    int id;
    char[][] pixels = new char[TILE_SIZE][TILE_SIZE];
    Set<String> allPossibleBorders;

    Tile(int id, List<String> data) {
        this.id = id;
        for (int row = 0; row < TILE_SIZE; row++) {
            pixels[row] = data.get(row).toCharArray();
        }
        this.allPossibleBorders = calculateAllPossibleBorders();
    }

    public Set<String> getAllPossibleBorders() {
        return allPossibleBorders;
    }

    char[] getBorder(int side) {
        return switch (side) {
            case TOP -> getTopBorder();
            case RIGHT -> getRightBorder();
            case BOTTOM -> getBottomBorder();
            case LEFT -> getLeftBorder();
            default -> null;
        };
    }

    char[] getTopBorder() {
        return pixels[0];
    }

    char[] getBottomBorder() {
        return pixels[TILE_SIZE - 1];
    }

    char[] getLeftBorder() {
        var leftBorder = new char[TILE_SIZE];
        for (int row = 0; row < TILE_SIZE; row++) {
            leftBorder[row] = pixels[row][0];
        }
        return leftBorder;
    }

    char[] getRightBorder() {
        var rightBorder = new char[TILE_SIZE];
        for (int row = 0; row < TILE_SIZE; row++) {
            rightBorder[row] = pixels[row][TILE_SIZE - 1];
        }
        return rightBorder;
    }

    private Set<String> calculateAllPossibleBorders() {
        var borders = new HashSet<String>();

        var topBorder = new String(getTopBorder());
        borders.add(topBorder);
        borders.add(new StringBuilder(topBorder).reverse().toString());
        var rightBorder = new String(getRightBorder());
        borders.add(rightBorder);
        borders.add(new StringBuilder(rightBorder).reverse().toString());
        var bottomBorder = new String(getBottomBorder());
        borders.add(bottomBorder);
        borders.add(new StringBuilder(bottomBorder).reverse().toString());
        var leftBorder = new String(getLeftBorder());
        borders.add(leftBorder);
        borders.add(new StringBuilder(leftBorder).reverse().toString());

        return borders;
    }

    void rotateCW() {
        char[][] rotated = new char[TILE_SIZE][TILE_SIZE];

        for (int row = 0; row < TILE_SIZE; ++row)
            for (int column = 0; column < TILE_SIZE; ++column)
                rotated[row][column] = pixels[TILE_SIZE - column - 1][row];

        pixels = rotated;
    }

    void flip() {
        char[][] flipped = new char[TILE_SIZE][TILE_SIZE];

        for (int row = 0; row < TILE_SIZE; row++) {
            flipped[TILE_SIZE - row - 1] = pixels[row];
        }
        pixels = flipped;

    }

    String getLine(int line) {
        return new String(pixels[line]);
    }

}
