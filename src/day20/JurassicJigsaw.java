package day20;

import util.FileReader;

import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class JurassicJigsaw {
    private static final int MATRIX_SIZE = 12;
    private static final int CROPPED_TILE_SIZE = Tile.TILE_SIZE - 2;
    private static final String MONSTER = """
                              #\s
            #    ##    ##    ###
             #  #  #  #  #  #  \s
            """;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day20/jurassicJigsaw.txt");

        var tiles = parseTiles(lines);
        var neighbourMap = convertToNeighbourMap(tiles);

        part1(neighbourMap);
        part2(neighbourMap, tiles);
    }

    private static void part1(Map<Integer, List<Integer>> neighbourMap) {
        var corners = getCorners(neighbourMap);
        var result = corners.stream().mapToLong(v -> v).reduce(1, (a, b) -> a * b);
        System.out.println("#1 Result: " + result);
    }

    private static void part2(Map<Integer, List<Integer>> neighbourMap, Map<Integer, Tile> tiles) {
        var matrix = buildMatrixOfIds(neighbourMap);
        rotateAndFlipTiles(tiles, matrix);

        var finalImage = createFinalImage(tiles, matrix);

        var total = finalImage.chars().filter(ch -> ch == '#').count();
        var seaMonsterSize = MONSTER.chars().filter(ch -> ch == '#').count();
        var seaMonstersCount = findMonsters(finalImage);

        var result = total - seaMonsterSize * seaMonstersCount;

        System.out.println("#2 Result: " + result);
    }

    private static long findMonsters(String image) {
        // find all mid lines (most complex) and check if above and below are matching too
        var pattern = Pattern.compile("#....##....##....###");
        var matcher = pattern.matcher(image);
        return matcher.results()
                .filter(mr -> {
                    // check line above
                    var lineAboveStart = mr.start() - CROPPED_TILE_SIZE * MATRIX_SIZE - 1;
                    var stringAbove = image.substring(lineAboveStart, lineAboveStart + 20);
                    var headPattern = Pattern.compile("..................#.");
                    var headMatcher = headPattern.matcher(stringAbove);
                    if (!headMatcher.matches()) {
                        return false;
                    }

                    //check line below
                    var lineBelowStart = mr.start() + CROPPED_TILE_SIZE * MATRIX_SIZE + 1;
                    var stringBelow = image.substring(lineBelowStart, lineBelowStart + 20);
                    var bodyPattern = Pattern.compile(".#..#..#..#..#..#...");
                    var bodyMatcher = bodyPattern.matcher(stringBelow);
                    return bodyMatcher.matches();
                })
                .count();
    }


    private static String createFinalImage(Map<Integer, Tile> tiles, Integer[][] matrix) {
        var image = new StringBuilder();
        for (int row = 0; row < MATRIX_SIZE; row++) {
            for (int tileRow = 0; tileRow < CROPPED_TILE_SIZE; tileRow++) {
                for (int column = 0; column < MATRIX_SIZE; column++) {
                    var tileLine = tiles.get(matrix[row][column]).getLine(tileRow + 1);
                    tileLine = tileLine.substring(1, Tile.TILE_SIZE - 1);
                    image.append(tileLine);
                }
                image.append("\n");
            }
        }
        return image.toString();
    }

    private static Map<Integer, Tile> parseTiles(List<String> lines) {
        var tileNumber = 0;
        var tilesMap = new HashMap<Integer, Tile>();
        for (int i = 0; i < lines.size(); i++) {
            var line = lines.get(i);
            if (line.isBlank()) {
                // do nothing
            } else if (line.contains("Tile")) {
                tileNumber = Integer.parseInt(line.substring(5, line.length() - 1));
            } else {
                var tileLines = new ArrayList<String>();
                for (int j = 0; j < day20.Tile.TILE_SIZE; j++) {
                    tileLines.add(lines.get(i));
                    i++;
                }
                tilesMap.put(tileNumber, new Tile(tileNumber, tileLines));
            }

        }

        return tilesMap;
    }

    private static Map<Integer, List<Integer>> convertToNeighbourMap(Map<Integer, Tile> tiles) {
        var neighbourCountMap = new HashMap<Integer, List<Integer>>();
        tiles.forEach((tileNumber, tile) -> {
            var neighbours = tiles.entrySet().stream()
                    .filter(tileEntry -> {
                        if (tileNumber.equals(tileEntry.getKey())) {
                            return false; // ignore itself
                        }
                        var intersection = new HashSet<>(tileEntry.getValue().getAllPossibleBorders());
                        intersection.retainAll(tile.getAllPossibleBorders());
                        return !intersection.isEmpty();
                    })
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            neighbourCountMap.put(tileNumber, neighbours);
        });
        return neighbourCountMap;
    }

    private static Set<Integer> getCorners(Map<Integer, List<Integer>> neighboursMap) {
        var corners = new HashSet<Integer>();
        neighboursMap.forEach((tileNumber, neighbours) -> {
            if (neighbours.size() == 2) {
                corners.add(tileNumber);
            }
        });
        return corners;
    }

    private static Integer[][] buildMatrixOfIds(Map<Integer, List<Integer>> neighboursMap) {
        var matrix = new Integer[MATRIX_SIZE][MATRIX_SIZE];

        // first set corner
        matrix[0][0] = neighboursMap.entrySet().stream()
                .filter(tile -> tile.getValue().size() == 2)
                .findAny().get().getKey();


        for (int row = 0; row < MATRIX_SIZE - 1; row++) {
            for (int column = 0; column < MATRIX_SIZE - 1; column++) {
                if (matrix[row][column + 1] == null) {
                    matrix[row][column + 1] = neighboursMap.get(matrix[row][column]).get(0);
                    neighboursMap.get(matrix[row][column + 1]).remove(matrix[row][column]);
                    neighboursMap.get(matrix[row][column]).remove(matrix[row][column + 1]);
                }
                if (matrix[row + 1][column] == null) {
                    matrix[row + 1][column] = neighboursMap.get(matrix[row][column]).get(0);
                    neighboursMap.get(matrix[row + 1][column]).remove(matrix[row][column]);
                    neighboursMap.get(matrix[row][column]).remove(matrix[row + 1][column]);
                }

                var intersection = new ArrayList<>(neighboursMap.get(matrix[row][column + 1]));
                var integers = neighboursMap.get(matrix[row + 1][column]);
                intersection.retainAll(integers);
                matrix[row + 1][column + 1] = intersection.get(0);

                neighboursMap.get(matrix[row + 1][column + 1]).remove(matrix[row + 1][column]);
                neighboursMap.get(matrix[row + 1][column + 1]).remove(matrix[row][column + 1]);
                neighboursMap.get(matrix[row][column + 1]).remove(matrix[row + 1][column + 1]);
                neighboursMap.get(matrix[row + 1][column]).remove(matrix[row + 1][column + 1]);
            }
        }
        return matrix;
    }

    private static void rotateAndFlipTiles(Map<Integer, Tile> tiles, Integer[][] matrix) {
        // find first (by trying all options with second)
        var first = tiles.get(matrix[0][0]);
        rotateAndFlipFirst(first, tiles.get(matrix[0][1]));
        // find flip of first by comparing with first in second row
        if (!rotateAndFlipToFit(tiles.get(matrix[1][0]), first, Tile.BOTTOM)) {
            first.flip();
        }

        // rotate and flip others by neighbour
        for (int row = 0; row < MATRIX_SIZE; row++) {
            for (int column = 0; column < MATRIX_SIZE; column++) {
                if (row == 0 && column == 0) {
                    continue; // ignore first - already rotated and flipped
                }
                if (column == 0) {
                    rotateAndFlipToFit(tiles.get(matrix[row][column]), tiles.get(matrix[row - 1][column]), Tile.BOTTOM);
                } else {
                    rotateAndFlipToFit(tiles.get(matrix[row][column]), tiles.get(matrix[row][column - 1]), Tile.RIGHT);
                }
            }
        }
    }

    private static void rotateAndFlipFirst(Tile first, Tile second) {
        rotateAndFlip(first, () -> rotateAndFlipToFit(second, first, Tile.RIGHT));
    }

    private static boolean rotateAndFlipToFit(Tile tileToFit, Tile anchor, int anchorSide) {
        return rotateAndFlip(tileToFit, () -> {
            var tileToFitSide = (anchorSide + 2) % 4;
            return Arrays.equals(anchor.getBorder(anchorSide), tileToFit.getBorder(tileToFitSide));
        });
    }

    private static boolean rotateAndFlip(Tile tile, Supplier<Boolean> checker) {
        for (int rotation = 0; rotation < 4; rotation++) {
            if (checker.get()) {
                return true;
            }
            tile.rotateCW();
        }
        tile.flip();
        if (checker.get()) {
            return true;
        }
        for (int rotation = 0; rotation < 4; rotation++) {
            if (checker.get()) {
                return true;
            }
            tile.rotateCW();
        }
        return false;
    }
}
