package day9;

import util.FileReader;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class EncodingError {
    public static final int PREAMBLE_SIZE = 25;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day9/encodingError.txt");
        var data = lines.stream().map(Long::parseLong).collect(Collectors.toList());

        part1(data);
        part2(data);
    }

    private static void part1(List<Long> data) {
        var incorrectNumber = findIncorrectNumber(data);
        System.out.println("#1 Result: " + data.get(incorrectNumber));
    }

    private static void part2(List<Long> data) {
        var incorrectNumber = findIncorrectNumber(data);
        var targetValue = data.get(incorrectNumber);

        for (int index = 0; index < incorrectNumber; index++) {
            var subList = data.subList(index, incorrectNumber);
            var rangeFound = checkRange(subList, targetValue);
            if (rangeFound != null) {
                System.out.println("#2 Result: " + getResultFromRange(rangeFound));
            }
        }
    }

    private static boolean checkValue(int index, List<Long> data) {
        // convert previous 25 numbers to set
        var preamble = new HashSet<>(data.subList(index - PREAMBLE_SIZE, index));
        // check if sum is present
        for (Long targetVal1 : preamble) {
            var targetVal2 = data.get(index) - targetVal1;
            if (preamble.contains(targetVal2)) {
                return true;
            }
        }
        return false;
    }

    private static int findIncorrectNumber(List<Long> data) {
        for (int index = PREAMBLE_SIZE; index < data.size(); index++) {
            if (!checkValue(index, data)) {
                return index;
            }
        }
        return 0;
    }

    private static List<Long> checkRange(List<Long> data, long targetVal) {
        var sum = 0;
        for (int index = 0; index < data.size(); index++) {
            var val = data.get(index);
            sum += val;
            if (sum == targetVal) {
                return data.subList(0, index + 1);
            }
            if (sum > targetVal) {
                return null;
            }
        }
        return null;
    }

    private static long getResultFromRange(List<Long> subList) {
        var sorted = subList.stream().sorted().collect(Collectors.toList());
        return sorted.get(0) + sorted.get(sorted.size() - 1);
    }
}
