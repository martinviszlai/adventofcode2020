package day21;

import util.FileReader;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AllergenAssessment {
    public static final Pattern LINE_PATTERN = Pattern.compile("(.+)\\(contains (.+)\\)");

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day21/allergenAssessment.txt");
        var allergens = parseAllergens(lines);

        part1(allergens, lines);
        part2(allergens);
    }

    private static void part1(Map<String, String> allergens, List<String> lines) {
        var ingredientsWithoutAllergen = getAllIngredients(lines);
        ingredientsWithoutAllergen.removeAll(allergens.values());

        System.out.println("#1 Result: " + ingredientsWithoutAllergen.size());
    }

    private static void part2(Map<String, String> allergens) {
        var result = allergens.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
        var resultString = String.join(",", result);

        System.out.println("#2 Result: " + resultString);
    }

    private static List<String> getAllIngredients(List<String> lines) {
        var allIngredients = new ArrayList<String>(); // use list to preserve entries with multiple occurrences
        lines.forEach(line -> {
            var matcher = LINE_PATTERN.matcher(line);
            if (matcher.matches()) {
                var ingredients = matcher.group(1).trim().split(" ");
                allIngredients.addAll(Arrays.asList(ingredients));
            }
        });
        return allIngredients;
    }

    private static Map<String, String> parseAllergens(List<String> lines) {
        var parsedMap = new HashMap<String, List<List<String>>>();
        lines.forEach(line -> {
            var matcher = LINE_PATTERN.matcher(line);
            if (matcher.matches()) {
                var ingredients = matcher.group(1).trim().split(" ");
                var allergens = matcher.group(2).split(", ");
                Arrays.stream(allergens).forEach(allergen -> {
                    var recipes = parsedMap.getOrDefault(allergen, new ArrayList<>());
                    recipes.add(Arrays.asList(ingredients));
                    parsedMap.put(allergen, recipes);
                });
            }
        });

        return findAllergens(parsedMap);
    }

    private static Map<String, String> findAllergens(Map<String, List<List<String>>> allergensMap) {
        // first round - remove all not applicable ingredients for allergen
        var reducedMap = new HashMap<String, Set<String>>();
        allergensMap.forEach((allergen, recipes) -> {
            var intersect = new HashSet<>(recipes.get(0));
            for (int i = 1; i < recipes.size(); i++) {
                intersect.retainAll(recipes.get(i));
            }
            reducedMap.put(allergen, intersect);
        });

        // second round - get final ingredients for allergen
        for (int i = 0; i < reducedMap.size(); i++) {
            reducedMap.forEach((allergen, ingredientsToRemove) -> {
                if (ingredientsToRemove.size() == 1) {
                    reducedMap.forEach((a, ingredients) -> {
                        if (!a.equals(allergen)) { // ignore itself
                            ingredients.removeAll(ingredientsToRemove);
                        }
                    });
                }
            });
        }

        // third round - unwrap collections
        var allergens = new HashMap<String, String>();
        reducedMap.forEach((allergen, ingredientSet) -> {
            var ingredients = new ArrayList<>(ingredientSet);
            allergens.put(allergen, ingredients.get(0));
        });
        return allergens;
    }
}
