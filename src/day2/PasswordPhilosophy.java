package day2;

import util.FileReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PasswordPhilosophy {
    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day2/passwordPhilosophy.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> data) {
        var matchingPasswords = data.stream()
                .filter(line -> {
                    var pd = extractPassData(line);

                    // for input '1-3 a: abcde' is regex: '([^a]*[a][^a]*){1,3}'
                    var regex = String.format("([^%s]*[%s][^%s]*){%d,%d}", pd.letter, pd.letter, pd.letter, pd.position.get(0), pd.position.get(1));
                    var matches = pd.pass.matches(regex);
//                    System.out.println(matches + " " + line);
                    return matches;
                }).count();

        System.out.println("#1 Matching passwords: " + matchingPasswords);
    }

    private static void part2(List<String> lines) {
        var matchingPasswords = lines.stream()
                .filter(line -> {
                    var pd = extractPassData(line);

                    var matches = pd.pass.charAt(pd.position.get(0) - 1) == pd.letter ^
                            pd.pass.charAt(pd.position.get(1) - 1) == pd.letter;
//                    System.out.println(matches + " " + line);
                    return matches;
                }).count();

        System.out.println("#2 Matching passwords: " + matchingPasswords);
    }

    private static PassData extractPassData(String data) {
        var strings = data.split(":");
        var rule = strings[0].split(" ");

        var passData = new PassData();
        passData.position = Arrays.stream(rule[0].split("-")).map(Integer::parseInt).collect(Collectors.toList());
        passData.letter = rule[1].charAt(0);
        passData.pass = strings[1].trim();

        return passData;
    }

    private static class PassData {
        String pass;
        char letter;
        List<Integer> position;
    }
}
