package day10;

import util.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class AdapterArray {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day10/adapterArray.txt");
        var data = lines.stream().map(Integer::parseInt).sorted().collect(Collectors.toList());

        part1(data);
        part2(data);
    }

    private static void part1(List<Integer> data) {
        var diffsMap = new HashMap<Integer, Integer>();
        //add one diff for initial adapter
        diffsMap.put(data.get(0), 1);

        for (int index = 1; index < data.size(); index++) {
            var diff = data.get(index) - data.get(index - 1);
            var diffsCount = 1;
            if (diffsMap.containsKey(diff)) {
                diffsCount = diffsMap.get(diff) + 1;
            }
            diffsMap.put(diff, diffsCount);
        }

        //add one additional diff for the device
        diffsMap.put(3, diffsMap.get(3) + 1);

        System.out.println("#1 Result: " + diffsMap.get(1) * diffsMap.get(3));
    }

    private static void part2(List<Integer> data) {
        var adapterAncestorOptionsCount = new HashMap<Integer, Long>();
        adapterAncestorOptionsCount.put(0, 1L); // add initial value

//   to 1 can get from 0                -> 1 ways
//   to 4 can get from 1                -> 1 ways
//   to 5 can get from 4                -> 1 ways
//   to 6 can get from 4(1),5(1)        -> 2 ways
//   to 7 can get from 4(1),5(1),6(2)   -> 4 ways
//   to 10 can get from 7(4)            -> 4 ways
//   to 11 can get from 10(4)           -> 4 ways
//   to 12 can get from 10(4),11(4)     -> 8 ways
//   to 15 can get from 12(8)           -> 8 ways
//   ...

        for (int index = 0; index < data.size(); index++) {
            var ancestors = getPossibleAncestors(index, data);
            var optionsCount = ancestors.stream().mapToLong(adapterAncestorOptionsCount::get).sum();
            adapterAncestorOptionsCount.put(data.get(index), optionsCount);
        }

        System.out.println("#2 Result: " + adapterAncestorOptionsCount.get(data.get(data.size() - 1)));
    }



    private static List<Integer> getPossibleAncestors(int index, List<Integer> data) {
        var val = data.get(index);
        var ancestors = new ArrayList<Integer>();
        for (int i = 1; i <= 3; i++) {
            var possibleOption = index - i;
            // handle beginning of list
            if (possibleOption < 0) {
                if (val <= 3) { // for low numbers, add ancestor 0 (default)
                    ancestors.add(0);
                }
                break;
            }
            if (val - data.get(possibleOption) <= 3) {
                ancestors.add(data.get(possibleOption));
            }
        }
        return ancestors;
    }
}
