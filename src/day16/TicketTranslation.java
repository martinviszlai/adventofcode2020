package day16;

import util.FileReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TicketTranslation {
    private static List<Rule> rules;
    private static List<Integer> myTicket;
    private static List<List<Integer>> tickets;


    public static void main(String[] args) {
        var fileReader = new FileReader();
        rules = loadRules(fileReader.readFile("./src/day16/ticketTranslation_rules.txt"));
        tickets = loadTickets(fileReader.readFile("./src/day16/ticketTranslation_tickets.txt"));
        myTicket = loadTickets(fileReader.readFile("./src/day16/ticketTranslation_myTicket.txt")).get(0);

        part1();
        part2();
    }

    private static void part1() {
        var invalidValues = new ArrayList<Integer>();
        tickets.forEach(ticket -> {
            for (Integer val : ticket) {
                if (!isValidForAnyRule(val)) {
                    invalidValues.add(val);
                    break;
                }
            }
        });
        System.out.println("#1 Result: " + invalidValues.stream().mapToInt(v -> v).sum());
    }

    private static void part2() {
        var validTickets = filterInvalidTickets(tickets);

        // init rules map
        var rulesMap = new HashMap<Integer, List<Rule>>();
        for (int index = 0; index < myTicket.size(); index++) {
            rulesMap.put(index, new ArrayList<>(rules));
        }

        reduceNotApplicableRules(validTickets, rulesMap);
        removeDuplicateRules(rulesMap);

        // calculate result
        var result = rulesMap.entrySet().stream()
                .filter(field -> field.getValue().get(0).name.contains("departure"))
                .mapToLong(field -> myTicket.get(field.getKey()))
                .reduce(1, (a, b) -> a * b);

        System.out.println("#2 Result: " + result);
    }

    private static ArrayList<List<Integer>> filterInvalidTickets(List<List<Integer>> tickets) {
        var validTickets = new ArrayList<List<Integer>>();
        tickets.forEach(ticket -> {
            var isValid = true;
            for (Integer val : ticket) {
                if (!isValidForAnyRule(val)) {
                    isValid = false;
                    break;
                }
            }
            if (isValid) {
                validTickets.add(ticket);
            }
        });
        return validTickets;
    }

    private static void reduceNotApplicableRules(ArrayList<List<Integer>> tickets, HashMap<Integer, List<Rule>> fieldMap) {
        tickets.forEach(ticket -> {
            for (int index = 0; index < ticket.size(); index++) {
                var applicableRules = fieldMap.get(index);
                for (Rule rule : rules) {
                    if (!rule.applyRule(ticket.get(index))) {
                        applicableRules.remove(rule);
                    }
                }
                fieldMap.put(index, applicableRules);
            }
        });
    }

    private static void removeDuplicateRules(HashMap<Integer, List<Rule>> fieldMap) {
        rules.forEach(rule -> {
            for (int i = 0; i < fieldMap.size(); i++) { // go through map
                if (fieldMap.get(i).size() == 1) { // find field with single rule - final rule
                    var finalFieldRule = fieldMap.get(i).get(0);
                    for (int j = 0; j < fieldMap.size(); j++) { // remove final rule from other fields
                        var rules = fieldMap.get(j);
                        if (rules.size() != 1) {
                            rules.remove(finalFieldRule);
                        }
                    }
                }
            }
        });
    }

    private static List<List<Integer>> loadTickets(List<String> lines) {
        var tickets = new ArrayList<List<Integer>>();
        lines.forEach(ticket -> tickets.add(Arrays.stream(ticket.split(",")).map(Integer::parseInt).collect(Collectors.toList())));
        return tickets;
    }

    private static List<Rule> loadRules(List<String> lines) {
        var rules = new ArrayList<Rule>();
        lines.forEach(rule -> {
            var matcher = Pattern.compile("(.+): (\\d+)-(\\d+) or (\\d+)-(\\d+)").matcher(rule.trim());
            if (matcher.matches()) {
                rules.add(new Rule(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5)));
            }
        });
        return rules;
    }

    private static boolean isValidForAnyRule(int val) {
        for (Rule rule : rules) {
            if (rule.applyRule(val)) {
                return true;
            }
        }
        return false;
    }

    private static class Rule {
        String name;
        int[] rangeValues = new int[4];

        public Rule(String name, String... rangeValues) {
            this.name = name;
            for (int index = 0; index < rangeValues.length; index++) {
                this.rangeValues[index] = Integer.parseInt(rangeValues[index].trim());
            }
        }

        public boolean applyRule(int value) {
            return value >= rangeValues[0] && value <= rangeValues[1]
                    || value >= rangeValues[2] && value <= rangeValues[3];
        }

        @Override
        public String toString() {
            return "Rule{" +
                    name + ": " +
                    rangeValues[0] + "-" + rangeValues[1] +
                    " or " + rangeValues[2] + "-" + rangeValues[3] +
                    '}';
        }
    }
}
