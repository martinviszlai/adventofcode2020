package day15;

import java.util.*;

public class RambunctiousRecitation {

    public static void main(String[] args) {
        var input = Arrays.asList(11,18,0,20,1,7,16);

        part1(input);
        part2(input);
    }

    private static void part1(List<Integer> startingNumbers) {
        var lastNumber = playTheGame(startingNumbers, 2020);
        System.out.println("#1 Result: " + lastNumber);
    }

    private static void part2(List<Integer> startingNumbers) {
        var lastNumber = playTheGame(startingNumbers, 30000000);
        System.out.println("#2 Result: " + lastNumber);
    }

    private static int playTheGame(List<Integer> startingNumbers, int turnLimit) {
        var allNumbersHistory = new HashMap<Integer, List<Integer>>(); // Map<number, List<turnsWhenSaid>>
        var lastNumber = 0;

        // init with input
        for (var turn = 0; turn < startingNumbers.size(); turn++) {
            addTurnToNumberHistory(turn, startingNumbers.get(turn), allNumbersHistory);
            lastNumber = startingNumbers.get(turn);
        }

        // play the game
        for (var turn = startingNumbers.size(); turn < turnLimit; turn++) {
            var numberHistory = allNumbersHistory.get(lastNumber);
            if (numberHistory.size() < 2) {
                lastNumber = 0;
            } else {
                lastNumber = numberHistory.get(numberHistory.size() - 1) - numberHistory.get(numberHistory.size() - 2);
            }
            addTurnToNumberHistory(turn, lastNumber, allNumbersHistory);
        }
        return lastNumber;
    }

    private static void addTurnToNumberHistory(int turn, int number, Map<Integer, List<Integer>> allNumbersHistory) {
        var numberHistory = allNumbersHistory.get(number);
        if (numberHistory == null) {
            numberHistory = new ArrayList<>();
        }
        numberHistory.add(turn);
        allNumbersHistory.put(number, numberHistory);
    }
}
