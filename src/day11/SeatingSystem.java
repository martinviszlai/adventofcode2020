package day11;

import util.FileReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SeatingSystem {
    public static final char OCCUPIED = '#';
    public static final char EMPTY = 'L';
    public static final char FLOOR = '.';
    public static int NEIGHBOURS_LIMIT = 4;
    public static int MAX_DISTANCE = 1;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var layout = fileReader.readFile("./src/day11/seatingSystem.txt");

        part1(layout);
        part2(layout);
    }

    private static void part1(List<String> layout) {
        var layoutAfterSeating = simulateSeating(layout);

        System.out.println("#1 Result: " + computeResult(layoutAfterSeating));
    }

    private static void part2(List<String> layout) {
        NEIGHBOURS_LIMIT = 5;
        MAX_DISTANCE = 100;

        var layoutAfterSeating = simulateSeating(layout);

        System.out.println("#2 Result: " + computeResult(layoutAfterSeating));
    }


    private static List<String> simulateSeating(List<String> layout){
        for (int i = 0; i < 200; i++) { // avoid infinite loop if algorithm doesn't work
            var afterIteration = applyRuleToLayout(layout);
            if (compareLayouts(layout, afterIteration)) {
                return afterIteration;
            } else {
                layout = afterIteration;
//                System.out.println();
            }
        }
        return Collections.emptyList();
    }

    private static int computeResult(List<String> layout) {
        var occupied = 0;
        for (String row : layout) {
            occupied += row.chars().filter(ch -> ch == OCCUPIED).count();
        }
        return occupied;
    }

    private static List<String> applyRuleToLayout(List<String> layout) {
        var rowsCount = layout.size();
        var columnsCount = layout.get(0).length();
        char[][] afterRule1 = new char[rowsCount][columnsCount];
        for (int row = 0; row < rowsCount; row++) {
            for (int column = 0; column < columnsCount; column++) {
                applyRuleToSingleSeat(row, column, layout, afterRule1);
            }
        }
        return convertToListOfStrings(afterRule1);
    }

    private static boolean compareLayouts(List<String> old, List<String> newLayout) {
        for (int index = 0; index < old.size(); index++) {
            var row = old.get(index);
            if (!row.equals(newLayout.get(index))) {
                return false;
            }
        }
        return true;
    }

    private static List<String> convertToListOfStrings(char[][] layout) {
        var result = new ArrayList<String>();
        for (char[] chars : layout) {
            var sb = new StringBuilder();
            sb.append(chars);
            result.add(sb.toString());
//            System.out.println(sb.toString());
        }
        return result;
    }

    private static void applyRuleToSingleSeat(int row, int column, List<String> originalLayout, char[][] newLayout) {
        var seat = originalLayout.get(row).charAt(column);
        if (seat == FLOOR) {
            newLayout[row][column] = FLOOR;
            return;
        }

        var occupiedNeighbours = 0;
        for (int rowDirection = -1; rowDirection <= 1; rowDirection++) { // can use binary arithmetics to define directions more efficiently
            for (int columnDirection = -1; columnDirection <= 1; columnDirection++) {
                if (rowDirection == 0 && columnDirection == 0) {
                    continue;
                }
                if (canSeeOccupied(row, column, rowDirection, columnDirection, MAX_DISTANCE, originalLayout)) {
                    occupiedNeighbours++;
                }
            }
        }
        switch (seat) {
            case EMPTY -> newLayout[row][column] = occupiedNeighbours == 0 ? OCCUPIED : seat;
            case OCCUPIED -> newLayout[row][column] = occupiedNeighbours >= NEIGHBOURS_LIMIT ? EMPTY : seat;
            default -> newLayout[row][column] = seat;
        }
    }

    private static boolean canSeeOccupied(int row, int column, int rowDirection, int columnDirection, int maxDistance, List<String> layout) {
        var targetRow = row;
        var targetColumn = column;
        var dist = 0;
        while (dist < maxDistance) {
            targetRow += rowDirection;
            targetColumn += columnDirection;
            if (targetRow < 0 || targetRow >= layout.size() || targetColumn < 0 || targetColumn >= layout.get(row).length()) {
                return false;
            }
            if (layout.get(targetRow).charAt(targetColumn) != FLOOR) {
                return layout.get(targetRow).charAt(targetColumn) == OCCUPIED;
            }
            dist++;
        }
        return false;
    }
}