package day25;

public class ComboBreaker {
    private static final int DOOR_PK = 2084668;
    private static final int CARD_PK = 3704642;

    public static void main(String[] args) {
        var cardLoopSize = findLoopSize(CARD_PK, 7);
        var encryptionKey = findEncryptionKey(cardLoopSize, DOOR_PK);

        System.out.println("Result: " + encryptionKey);

    }

    private static long findEncryptionKey(int loopSize, int subjectNumber) {
        var value = 1L;
        for (int i = 0; i < loopSize; i++) {
            value *= subjectNumber;
            value %= 20201227;
        }
        return value;
    }

    private static int findLoopSize(long publicKey, int subjectNumber) {
        var value = 1L;
        var loopSize = 0;
        while (value != publicKey) {
            loopSize++;
            value *= subjectNumber;
            value %= 20201227;
        }
        return loopSize;
    }

}
