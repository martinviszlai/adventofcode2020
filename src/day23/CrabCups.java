package day23;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CrabCups {
    public static final String INPUT = "589174263";

    public static void main(String[] args) {
        var cups = INPUT.chars().mapToObj(c -> c - '0').collect(Collectors.toCollection(LinkedList::new));

        part1(cups);
        part2(cups);
    }

    private static void part1(LinkedList<Integer> initialCups) {
        var cups = init(initialCups);
        var cupOne = getCupByLabel(1, cups);

        playCrabCups(cups.get(0), 100);

        var sb = new StringBuilder();
        var currentCup = cupOne.next;
        for (int i = 1; i < 9; i++) {
            sb.append(currentCup.label);
            currentCup = currentCup.next;
        }

        System.out.println("#1 Result: " + sb.toString());
    }

    private static void part2(LinkedList<Integer> initialCups) {
        var cups = init(initialCups);
        var cupOne = getCupByLabel(1, cups);

        addCups(cups, cupOne);
        playCrabCups(cups.get(0), 10_000_000);

        var result = (long) cupOne.next.label * (long) cupOne.next.next.label;

        System.out.println("#2 Result: " + result);
    }

    private static void addCups(List<Cup> cups, Cup cupOne) {
        var cupNine = getCupByLabel(9, cups);
        var firstCup = cups.get(0);
        var lastCup = cups.get(cups.size() - 1);
        var label = 10;
        var cup = new Cup(label++);
        cup.prev = lastCup;
        cup.destinationCup = cupNine;
        lastCup.next = cup;
        lastCup = cup;
        cups.add(cup);
        while (cups.size() < 1_000_000) {
            cup = new Cup(label++);
            cup.prev = lastCup;
            cup.destinationCup = lastCup;
            lastCup.next = cup;
            lastCup = cup;
            cups.add(cup);
        }
        cupOne.destinationCup = lastCup;
        lastCup.next = firstCup;
        firstCup.prev = lastCup;
    }

    private static Cup getCupByLabel(int targetLabel, List<Cup> cups) {
        var cup = cups.get(0);
        while (cup.label != targetLabel) {
            cup = cup.next;
        }
        return cup;
    }

    private static List<Cup> init(List<Integer> list) {
        var cups = new ArrayList<Cup>();
        for (Integer cupLabel : list) {
            cups.add(new Cup(cupLabel));
        }
        for (int i = 0; i < cups.size(); i++) {
            var cup = cups.get(i);
            cup.prev = cups.get((cups.size() + i - 1) % cups.size());
            cup.next = cups.get((i + 1) % cups.size());
            var minusIndex = list.indexOf(cup.label - 1);
            if (minusIndex < 0) {
                minusIndex = list.indexOf(9);
            }
            cup.destinationCup = cups.get(minusIndex);
        }
        return cups;
    }

    private static Cup playCrabCups(Cup currentCup, int moves) {
        for (int move = 1; move <= moves; move++) {
            var printCup = currentCup;
            for (int i = 0; i < 9; i++) {
                printCup = printCup.next;
            }

            // pick 3 cups
            var picked1 = currentCup.next;
            var picked2 = picked1.next;
            var picked3 = picked2.next;

            currentCup.next = picked3.next;
            picked3.next.prev = currentCup;

            // find dest cup
            var destinationCup = currentCup.destinationCup;
            while (destinationCup == picked1 || destinationCup == picked2 || destinationCup == picked3) {
                destinationCup = destinationCup.destinationCup;
            }

            // add 3 cups back
            picked1.prev = destinationCup;
            picked3.next = destinationCup.next;
            destinationCup.next.prev = picked3;
            destinationCup.next = picked1;

            // find current cup
            currentCup = currentCup.next;
        }
        return currentCup;
    }
}
