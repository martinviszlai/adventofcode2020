package day23;

public class Cup {
    Cup prev;
    Cup next;
    Cup destinationCup;
    int label;

    Cup(int label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Cup{" + label + '}';
    }
}
