package day24;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Tile {
    int x;
    int y;
    int z;

    Tile(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    List<Tile> getNeighbours(){
        var neighbours = new ArrayList<Tile>();
        neighbours.add(new Tile(x+1, y, z-1));
        neighbours.add(new Tile(x+1, y-1, z));
        neighbours.add(new Tile(x, y-1, z+1));
        neighbours.add(new Tile(x-1, y, z+1));
        neighbours.add(new Tile(x-1, y+1, z));
        neighbours.add(new Tile(x, y+1, z-1));
        return neighbours;
    }

    @Override
    public String toString() {
        return "Tile{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return x == tile.x &&
                y == tile.y &&
                z == tile.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
