package day24;

import util.FileReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LobbyLayout {


    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day24/lobbyLayout.txt");

        var tiles = loadTiles(lines);

        part1(tiles);
        part2(tiles);
    }

    private static void part1(Map<Tile, Boolean> tiles) {
        long blackCount = getBlackCount(tiles);
        System.out.println("#1 Result: " + blackCount);
    }


    private static void part2(Map<Tile, Boolean> floor) {
        for (int day = 0; day < 100; day++) {
            floor = changeState(floor);
        }

        System.out.println("#2 Result: " + getBlackCount(floor));

    }

    private static HashMap<Tile, Boolean> changeState(Map<Tile, Boolean> floor) {
        var extendedFloor = new HashMap<Tile, Boolean>();
        // make sure all neighbour white tiles are in map
        floor.forEach((tile, isBlack) -> {
            extendedFloor.put(tile, isBlack);
            var neighbours = tile.getNeighbours();
            neighbours.forEach(neighbour -> {
                var neighbourIsBlack = floor.getOrDefault(neighbour, false);
                extendedFloor.put(neighbour, neighbourIsBlack);
            });
        });


        var newState = new HashMap<Tile, Boolean>();
        extendedFloor.forEach((tile, isBlack) -> {
            var blackNeighbours = countBlackNeighbours(tile, extendedFloor);
            //rule 1
            if (isBlack && (blackNeighbours == 0 || blackNeighbours > 2)) {
                newState.put(tile, false);
            }
            // rule 2
            else if (!isBlack && blackNeighbours == 2) {
                newState.put(tile, true);
            }
            // default
            else {
                newState.put(tile, extendedFloor.get(tile));
            }
        });
        return newState;
    }


    private static long getBlackCount(Map<Tile, Boolean> floor) {
        return floor.values().stream()
                .filter(isBlack -> isBlack)
                .count();
    }

    private static int countBlackNeighbours(Tile tile, Map<Tile, Boolean> floor) {
        int blackNeighbours = 0;
        for (Tile neighbour : tile.getNeighbours()) {
            var isBlack = floor.getOrDefault(neighbour, false);
            if (isBlack) {
                blackNeighbours++;
            }
        }
        return blackNeighbours;
    }


    // cube representation: https://www.redblobgames.com/grids/hexagons/#coordinates
    private static Map<Tile, Boolean> loadTiles(List<String> lines) {
        var tiles = new HashMap<Tile, Boolean>();
        lines.forEach(line -> {
            var chars = line.toCharArray();
            var x = 0;
            var y = 0;
            var z = 0;
            for (int i = 0; i < chars.length; i++) {
                switch (chars[i]) {
                    case 'e' -> {
                        x++;
                        y--;
                    }
                    case 's' -> {
                        z++;
                        if (chars[++i] == 'e') {
                            y--;
                        } else {
                            x--;
                        }
                    }
                    case 'w' -> {
                        x--;
                        y++;
                    }
                    case 'n' -> {
                        z--;
                        if (chars[++i] == 'e') {
                            x++;
                        } else {
                            y++;
                        }
                    }

                }
            }
            var tile = new Tile(x, y, z);
            tiles.put(tile, !tiles.getOrDefault(tile, false));
        });
        return tiles;
    }
}
