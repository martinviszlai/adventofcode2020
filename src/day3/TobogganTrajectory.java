package day3;

import util.FileReader;

import java.util.List;

public class TobogganTrajectory {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day3/tobogganTrajectory.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> mapData) {
        var encounters = checkSlope(mapData, 3, 1);

        System.out.println("#1 Result: " + encounters);
    }

    private static void part2(List<String> mapData) {
        var slope1 = checkSlope(mapData, 1, 1);
        var slope2 = checkSlope(mapData, 3, 1);
        var slope3 = checkSlope(mapData, 5, 1);
        var slope4 = checkSlope(mapData, 7, 1);
        var slope5 = checkSlope(mapData, 1, 2);

        System.out.println("#2 Result: " + slope1 * slope2 * slope3 * slope4 * slope5);
    }

    private static int checkSlope(List<String> mapData, int right, int down) {
        var encounters = 0;
        var positionInMap = 0;
        for (var index = 0; index < mapData.size(); index++) {
            if (index % down > 0) {
                continue;
            }
            var line = mapData.get(index);

            if (line.charAt(positionInMap) == '#') {
                encounters++;
            }

//            debugLog(line, positionInMap);

            positionInMap = (positionInMap + right) % line.length();
        }

//        System.out.println(String.format("For slope [%d;%d] total encounters: %d", right, down, encounters));
        return encounters;
    }

    private static void debugLog(String line, int positionInMap) {
        System.out.println(line);
        System.out.println(" ".repeat(positionInMap) + "^");
    }

}
