package day13;

import util.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShuttleSearch {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day13/shuttleSearch.txt");
        var departureTime = Integer.parseInt(lines.get(0));
        var busMap = getBuses(lines.get(1).split(","));
        var busList = getBusesList(lines.get(1).split(","));

        part1(departureTime, busList);
        part2(busList, busMap);
    }

    private static void part1(int departureTime, List<Integer> buses) {
        var departures = new HashMap<Integer, Integer>();
        buses.forEach(bus -> departures.put(-(departureTime % bus) + bus, bus));
        var minWaitTime = departures.keySet().stream().mapToInt(v -> v).min().getAsInt();
        var bestBus = departures.get(minWaitTime);

        System.out.println("#1 Result: " + minWaitTime * bestBus);
    }

    private static void part2(List<Integer> busList, Map<Integer, Integer> busMap) {
        long increment = busList.get(0); // holds value at which pattern repeats
        long time = busList.get(0);

        for (int i = 1; i < busList.size(); i++) {
            var nextBus = busList.get(i); // for each bus (starting with second)
            while ((time + busMap.get(nextBus)) % nextBus != 0) { // search for departure time expected by rules
                time += increment;
            }
            increment *= nextBus; // found departure time for bus. Set increment as Least Common Multiple of all busses combined
        }
        System.out.println("#2 Result: " + time);
    }

    public static Map<Integer, Integer> getBuses(String[] input) {
        var buses = new HashMap<Integer, Integer>();
        for (int i = 0; i < input.length; i++) {
            if (!input[i].equals("x")) {
                buses.put(Integer.parseInt(input[i]), i);
            }
        }
        return buses;
    }

    public static List<Integer> getBusesList(String[] input) {
        var buses = new ArrayList<Integer>();
        for (String s : input) {
            if (!s.equals("x")) {
                buses.add(Integer.parseInt(s));
            }
        }
        return buses;
    }
}
