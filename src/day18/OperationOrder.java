package day18;

import util.FileReader;

import java.util.ArrayList;
import java.util.List;

public class OperationOrder {
    private static final char ADD = '+';
    private static final char MULTIPLY = '*';
    private static final char PAREN_START = '(';
    private static final char PAREN_END = ')';


    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day18/operationOrder.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> lines) {
        var results = new ArrayList<Long>();
        lines.forEach(line -> {
            results.add(eval(line, false));
        });
        System.out.println("#1 Result: " + results.stream().mapToLong(v -> v).sum());
    }

    private static void part2(List<String> lines) {
        var results = new ArrayList<Long>();
        lines.forEach(line -> {
            results.add(eval(line, true));
        });
        System.out.println("#2 Result: " + results.stream().mapToLong(v -> v).sum());
    }


    private static long eval(String exp, boolean part2) {
        var opStack = new char[255];
        var rpnStack = new char[255];
        var pointer = rpnStack.length;
        var opPointer = -1;

        // convert to RPN
        // algorithm used: https://en.wikipedia.org/wiki/Shunting-yard_algorithm

        for (char element : exp.toCharArray()) {
            switch (element) {
                case ' ' -> {
                }
                case PAREN_START -> opStack[++opPointer] = element;
                case PAREN_END -> {
                    while (opStack[opPointer] != PAREN_START) {
                        rpnStack[--pointer] = opStack[opPointer--];
                    }
                    opPointer--;
                }
                case ADD, MULTIPLY -> {
                    while (opPointer >= 0 &&
                            (!part2 || opStack[opPointer] == ADD) &&
                            opStack[opPointer] != PAREN_START) {
                        rpnStack[--pointer] = opStack[opPointer--];
                    }
                    opStack[++opPointer] = element;
                }
                default -> rpnStack[--pointer] = element;
            }
        }

        System.arraycopy(opStack, 0, rpnStack, pointer - (opPointer + 1), opPointer + 1);

        return evalRpn(rpnStack, pointer - (opPointer + 1));
    }

    private static long evalRpn(char[] rpn, int size) {
        var result = new long[1 << 8];
        var pointer = -1;

        for (int prnPointer = rpn.length - 1; prnPointer >= size; prnPointer--) {
            switch ((int) rpn[prnPointer]) {
                case ADD -> result[--pointer] += result[pointer + 1];
                case MULTIPLY -> result[--pointer] *= result[pointer + 1];
                default -> result[++pointer] = rpn[prnPointer] - '0';
            }
        }

        return result[0];
    }
}


