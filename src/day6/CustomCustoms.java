package day6;

import util.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomCustoms {

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day6/customCustoms.txt");
        var groups = loadGroups(lines);

        part1(groups);
        part2(groups);
    }

    private static void part1(List<Group> groups) {
        var sum = groups.stream()
                .mapToInt(group -> group.answers.keySet().size())
                .sum();

        System.out.println("#1 Result: " + sum);
    }

    private static void part2(List<Group> groups) {
        var sum = groups.stream()
                .mapToLong(group -> group.answers.values().stream().filter(count -> count == group.groupSize).count())
                .sum();

        System.out.println("#2 Result: " + sum);
    }

    public static List<Group> loadGroups(List<String> data) {
        var groups = new ArrayList<Group>();
        var group = new Group();
        for (String line : data) {
            if (line.isBlank()) {
                groups.add(group);
                group = new Group();
            } else {
                group.groupSize++;
                for (char answer : line.toCharArray()) {
                    var answersCount = 1;
                    if (group.answers.containsKey(answer)) {
                        answersCount = group.answers.get(answer) + 1;
                    }
                    group.answers.put(answer, answersCount);
                }
            }
        }
        return groups;
    }

    private static class Group {
        int groupSize = 0;
        Map<Character, Integer> answers = new HashMap<>();
    }
}
