package day8;

import util.FileReader;

import java.util.HashSet;
import java.util.List;

public class HandheldHalting {
    private static int acc = 0;

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day8/handheldHalting.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> instructions) {

        executeProgram(instructions);

        System.out.println("#1 Result: " + acc);

    }

    private static void part2(List<String> instructions) {
        for (int index = 0; index < instructions.size(); index++) {
            if (instructions.get(index).contains("acc")) {
                continue;
            }
            if (tryToFix(instructions, index)) {
                break;
            }
        }
        System.out.println("#2 Result: " + acc);

    }

    private static boolean tryToFix(List<String> instructions, int index) {
        switchOperation(instructions, index);
        if (executeProgram(instructions)) {
            return true;
        }
        // issue not fixed, revert change
        switchOperation(instructions, index);
        return false;
    }

    private static void switchOperation(List<String> instructions, int index) {
        var instruction = instructions.get(index);
        var source = instruction.contains("jmp") ? "jmp" : "nop";
        var target = instruction.contains("jmp") ? "nop" : "jmp";
        instructions.remove(index);
        instructions.add(index, instruction.replace(source, target));
    }

    private static boolean executeProgram(List<String> instructions) {
        acc = 0;
        var executedInstructions = new HashSet<Integer>();
        var nextInstruction = 0;

        while (!executedInstructions.contains(nextInstruction)) {
            executedInstructions.add(nextInstruction);
            nextInstruction = executeInstruction(instructions.get(nextInstruction), nextInstruction);

            if (nextInstruction > instructions.size() - 1) {
                return true;
            }
        }
        return false;
    }

    private static int executeInstruction(String instruction, int index) {
        var split = instruction.split(" ");
        var val = Integer.parseInt(split[1]);
        switch (split[0]) {
            case "acc":
                acc += val;
                return index + 1;
            case "jmp":
                return index + val;
            default:
                return index + 1;
        }
    }
}
