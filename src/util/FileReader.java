package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReader {
    public List<String> readFile(String path) {
        var lines = new ArrayList<String>();
        try (BufferedReader reader = new BufferedReader(new java.io.FileReader(path))) {
            var line = reader.readLine();
            while (line != null) {
                lines.add(line);
//                System.out.println(line);
                // read next line
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }
}
