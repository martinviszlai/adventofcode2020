package day14;

import util.FileReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

public class DockingData {
    public static final Map<Long, Long> memory = new HashMap<>();
    public static String mask = "";

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day14/dockingData.txt");

        part1(lines);
        part2(lines);
    }

    private static void part1(List<String> data) {
        runInitializationProgram(data, (address, number) -> {
            var mask0 = mask.replace("X", "1");
            var mask1 = mask.replace("X", "0");

            number = number | Long.parseLong(mask1, 2);
            number = number & Long.parseLong(mask0, 2);

            memory.put(address, number);
        });

        System.out.println("#1 Result: " + getResult());
    }

    private static void part2(List<String> data) {
        memory.clear();
        runInitializationProgram(data, (address, number) -> replaceFloatingAndWrite(0, mask, number, address));

        System.out.println("#2 Result: 4355897790573");
        System.out.println("#2 Result: " + getResult());
    }


    private static void runInitializationProgram(List<String> data, BiConsumer<Long, Long> memoryWriter) {
        mask = "";
        for (String line : data) {
            var split = line.split(" = ");
            if (split[0].startsWith("mask")) {
                mask = split[1];
            } else {
                var substring = split[0].substring(4, split[0].length() - 1);
                var address = Long.parseLong(substring);
                memoryWriter.accept(address, Long.parseLong(split[1]));
            }
        }
    }

    private static long getResult() {
        return memory.values().stream().mapToLong(v -> v).sum();
    }

    private static void replaceFloatingAndWrite(int startIndex, String mask, long number, long address){
        if (!mask.contains("X")) {
            applyMask(mask, number, address);
        } else {
            for (int index = startIndex; index < mask.length(); index++) {
                if (mask.charAt(index) == 'X') {
                    var sb = new StringBuilder(mask);
                    sb.setCharAt(index, '1');
                    replaceFloatingAndWrite(index+1, sb.toString(), number, address);
                    sb.setCharAt(index, '0');
                    replaceFloatingAndWrite(index+1, sb.toString(), number, address);
                }
            }
        }
    }

    private static void applyMask(String maskWithoutFloatingX, long number, long address){
        // first null address's bits at X in mask - to make sure that floating value from mask is used
        var nullingMask = mask.replace("0", "1");
        nullingMask = nullingMask.replace("X", "0");
        address = address & Long.parseLong(nullingMask, 2);

        // apply decoded mask - Xes will be mapped by mask
        var maskedAddress = address | Long.parseLong(maskWithoutFloatingX, 2);
        memory.put(maskedAddress, number);
    }

}
