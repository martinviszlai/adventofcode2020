package day4;

import util.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class PassportProcessing {
    public static final Map<String, Predicate<String>> FIELD_VALIDATIONS = new HashMap<>() {{
        put("byr", val -> {
            if (!val.matches("\\d{4}")) {
                return false;
            }
            var intVal = Integer.parseInt(val);
            return intVal >= 1920 && intVal <= 2002;
        });
        put("iyr", val -> {
            if (!val.matches("\\d{4}")) {
                return false;
            }
            var intVal = Integer.parseInt(val);
            return intVal >= 2010 && intVal <= 2020;
        });
        put("eyr", val -> {
            if (!val.matches("\\d{4}")) {
                return false;
            }
            var intVal = Integer.parseInt(val);
            return intVal >= 2020 && intVal <= 2030;
        });
        put("hgt", val -> {
            if (!val.matches("\\d+(cm|in)")) {
                return false;
            }
            var height = Integer.parseInt(val.substring(0, val.length() - 2));
            if (val.contains("cm")) {
                return height >= 150 && height <= 193;
            } else {
                return height >= 59 && height <= 76;
            }
        });
        put("hcl", val -> val.matches("#[0-9a-f]{6}"));
        put("ecl", val -> val.matches("amb|blu|brn|gry|grn|hzl|oth"));
        put("pid", val -> val.matches("\\d{9}"));
    }};

    public static void main(String[] args) {
        var fileReader = new FileReader();
        var lines = fileReader.readFile("./src/day4/passportProcessing.txt");
        var passports = loadPassports(lines);

        part1(passports);
        part2(passports);
    }

    private static void part1(List<Map<String, String>> passports) {
        var valid = passports.stream().filter(passport -> passport.keySet().containsAll(FIELD_VALIDATIONS.keySet())).count();

        System.out.println("#1 Result: " + valid);
    }

    private static void part2(List<Map<String, String>> passports) {
        var valid = passports.stream().filter(passport -> {
            try {
                FIELD_VALIDATIONS.forEach((key, val) -> {
                    if (!passport.containsKey(key) || !val.test(passport.get(key))) {
                        throw new RuntimeException(String.format("Validation for '%s' failed. Value: %s", key, passport.get(key)));
                    }
                });
            } catch (RuntimeException ex) {
//            System.out.println(ex.getMessage());
                return false;
            }
            return true;
        }).count();

        System.out.println("#2 Result: " + valid);
    }

    private static List<Map<String, String>> loadPassports(List<String> data) {
        var passports = new ArrayList<Map<String, String>>();
        var current = new HashMap<String, String>();
        for (String line : data) {
            if (line.isBlank()) {
                passports.add(current);
                current = new HashMap();
            } else {
                var strings = line.split(" ");
                for (String string : strings) {
                    var field = string.split(":");
                    current.put(field[0], field[1]);
                }

            }
        }
        return passports;
    }

}
